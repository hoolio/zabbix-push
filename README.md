# Deploy zabbix agent to nodes using ansible

1. add a named user to those nodes allowing ssh auth from keys provided in authorized_keys
1. Enable reporting on apt packages (debian/ubuntu only) using

## Supported systems
* this is tested and works on debian (jessie) and ubuntu (trusty,xenial) nodes
* reporting to a zabbix server running 3.4.4 (we run ours on ubuntu)

NOTE: [Zabbix provide downloads](https://www.zabbix.com/download) for other systems, you'll need to change logic and URLs in push-zabbix-agent.yml

## Prerequisites
1. A working understanding of Zabbix, ansible and git
1. A functioning Zabbix environment

## Installation
1. Install ansible from the ansible repositories on your deployment node.  I just use the zabbix server itself.
1. git clone this repository onto your deployment node.
1. in `zabbix_agentd.conf` change the `Server` and `ServerActive` to the ip address of your Zabbix server
1. get the [zabbix-apt](https://github.com/theranger/zabbix-apt) repository with `git submodule init` and `git submodule update`
1. setup `/etc/ansible/hosts` as per normal
1. push changes to nodes with `ansible-playbook push-zabbix-agent.yml`
1. make sure nodes are listening to your zabbix server on port 10051

## Auto registration of zabbix agents (optional)
1. As per the [zabbix doco](https://www.zabbix.com/documentation/3.4/manual/discovery/auto_registration), this configured on the zabbix server
1. This config will have agents report with the string "Linux"

## Setup of zabbix-apt (also optional)
The node config of [zabbix-apt](https://github.com/theranger/zabbix-apt) is setup automatically by `push-zabbix-agent.yml` so leaving only
> 3. Import templates/apt-updates.xml to Zabbix frontend.

## Troubleshooting Tips
I find it useful to run two ssh sessions to the zabbix server:
1. `$ telnet xxx.xxx.xxx.xxx 10050` after installation to check your agent is listening
1. `$ sudo tail -f /var/log/zabbix/zabbix_server.log` to see auto registered nodes appear

## Appendix
Git submodules discussed at https://git-scm.com/book/en/v2/Git-Tools-Submodules
